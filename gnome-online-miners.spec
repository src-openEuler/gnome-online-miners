Name:           gnome-online-miners
Version:        3.34.0
Release:        5
Summary:        Provide the user to grab the online date function
License:        GPLv2+ and LGPLv2+ and MIT
URL:            https://wiki.gnome.org/Projects/GnomeOnlineMiners
Source0:        https://download.gnome.org/sources/gnome-online-miners/3.34/gnome-online-miners-%{version}.tar.xz
BuildRequires:  gfbgraph-devel glib2-devel >= 2.35.1 gnome-online-accounts-devel >= 3.8.0 grilo-devel >= 0.3.0
BuildRequires:  libgdata-devel >= 0.15.2 libzapojit-devel >= 0.0.2 pkgconfig tracker3-devel >= 3.0.4 chrpath
Requires:       dbus grilo-plugins gvfs >= 1.18.3

Patch0:         tracker3.patch
Patch1:         gnome-online-miners-3.34.0-sw.patch
BuildRequires:  autoconf automake libtool
BuildRequires:  autoconf-archive

%description
The GNOME Online Miners provides a kind of crawlers to go through your online
content and index them locally in Tracker.It has miners for Facebook, Flickr,
Google, OneDrive and Nextcloud.

%prep
#autosetup -n gnome-online-miners-%{version} -p1
%autosetup -p1

%build
autoreconf -fi

%configure  --disable-silent-rules  --disable-static --disable-owncloud --disable-windows-live
%make_build

%install
%make_install
%delete_la

chrpath -d %{buildroot}%{_libexecdir}/*
mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo "%{_libdir}/gnome-online-miners" > %{buildroot}%{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc AUTHORS COPYING NEWS README
%{_datadir}/dbus-1/services/*.service
%{_libdir}/gnome-online-miners/libgom-1.0.so
%{_libexecdir}/*
%{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf
%exclude %{_docdir}/gnome-online-miners/ChangeLog

%changelog
* Thu Jun 20 2024 panchenbo <panchenbo@kylinsec.com.cn> - 3.34.0-5
- del sw_64 patch ifarch

* Thu Oct 20 2022 wuzx<wuzx1226@qq.com> - 3.34.0-4
- add sw64 patch

* Fri Sep 24 2021 Wenlong Ding <wenlong.ding@turbolinux.com.cn> - 3.34.0-3
- Add tracker3.patch

* Thu Sep 09 2021 lingsheng <lingsheng@huawei.com> - 3.34.0-2
- Delete rpath setting

* Thu Jun 17 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.34.0-1
- Upgrade to 3.34.0

* Thu Jun 18 2020 yaokai <yaokai13@huawei.com> - 3.30.0-2
- Package init
